<?php
namespace Core;
require_once "core/autoloader.php";
use Core\Autolader;
use Core\Request;
use Exception;

class Framework{
    
    public static function check(){
        
        $controllerName = Request::getControllerName();
        $ClassName = 'controllers\\' . $controllerName;
        $methodName = Request::getMethodName();
        $params = Request::getParams();
        
        Autoloader::load();

        try{

            if($controllerName == ""){
                throw new Exception("There is nothing to show ):");
            }

            if($controllerName != ""){
            
                if (class_exists($ClassName)) {
                    $class = new $ClassName; 
                    if (method_exists($ClassName,$methodName)) {
                        $class -> $methodName($params);
                    
                    } else if($methodName == '') {
                        $class -> index();
                    } else {
                        throw new Exception("Class" . " " . $controllerName . " " . "does not have method of" . " " . $methodName . " " . "name");
                    }
                } else {
                    throw new Exception( "Class" . " " . $controllerName . " " . "does not exist");
                }
            }
        }
        catch(Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }    
}