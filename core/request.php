<?php

namespace Core;

class Request{

    private static $instance;
   
    public static function getUrl(){
        
        $url = explode("/", $_SERVER["REQUEST_URI"]);
        $controllerName = $url[1];
        $methodName = $url[2];
        $params = [];
        
        for ($i = 3; $i < count($url); $i++) {
            if ($url[$i]) {
                $params[] = $url[$i];
            }
        }
        
        $info = [$controllerName,$methodName,$params];
        
        return $info;
        
    } 

    
    public static function getControllerName(){
        return (self::getUrl()[0]);
    }

    public static function getMethodName(){
        return (self::getUrl()[1]);
    }

    public static function getParams(){
        return (self::getUrl()[2]);
    }
    public static function getInstance() {
        return static::$instance ?? (static::$instance = new static);
    }


    private function __constract() {}
    private function __clone() {}
    private function __wakeup() {}
}

$instance=Request::getInstance();
