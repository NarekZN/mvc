<?php
namespace Core;

require_once("core/request.php");

class Autoloader {

    public static function load() {
      if (file_exists("controllers/". Request::getControllerName().".php")) {
            spl_autoload_register(function ($Controller) {
                require_once $Controller.".php";
            }); 
        }
    }

}